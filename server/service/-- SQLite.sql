-- SQLite
CREATE TABLE cart_item (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   product_id INTEGER NOT NULL,
   product_id INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS "membership" (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(200) NULL,
  `grade` VARCHAR(10) NOT NULL,
  `price` double NOT NULL);

CREATE TABLE IF NOT EXISTS "product" (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(200) NULL,
  `type` VARCHAR(5) NOT NULL,
  `price` double NOT NULL);
