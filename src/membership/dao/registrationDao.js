import config from '../../config/config.json';
import axios from 'axios';

export const registerMember = (data, selected) => {
    let url = "/register";
    if (selected.id) url = url + `?membership=${selected.id}`;

    const bodyOptions = {
        membership_id: selected.id,
        name: data.name,
        email: data.email,
        address: data.address
    }

    const headers = {
        headers: {}
    }

    return axios.post(config.SERVER_ENDPOINT + url, bodyOptions, headers)
        .then((res) => {
            console.log("🚀 ~ file: registrationDao.js ~ line 19 ~ .then ~ res", res)
            return res.data.data
        }).catch((err) => {
            console.log("API ERROR");
        });
}