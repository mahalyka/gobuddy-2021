import { useState } from 'react';
import '../style/membershipStyle.css';
import { Link } from 'react-router-dom'

export const MembershipItem = ({ id, name, description, price, onRegister }) => {

  const gradeData = {
    id,
    name,
    description,
    price
  }

  return (
    <div key={id} className='card card-body square'>
      <div className="mt-2 h4">{name}</div>
      <p>{description}</p>
      {/* <p className="price">${price}</p> */}
      {/* <br /> */}
      <Link className="button" to="/membership" onClick={(e) => onRegister(e, gradeData)}>Register for ${price}</Link>
    </div>
  );
}
