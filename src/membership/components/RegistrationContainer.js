import { useState, useEffect } from 'react';
import '../style/membershipStyle.css';
import { registerMember } from '../dao/registrationDao';


export const RegistrationContainer = ({ selected }) => {
    console.log("🚀 ~ file: RegistrationContainer.js ~ line 5 ~ RegistrationContainer ~ selected", selected)
    //React useState hook is used for state management. Utilzes setter setMembership to update product state
    const [values, setValues] = useState();


    const handleSubmit = (e, data) => {
        e.preventDefault()
        console.log(data)
        registerMember(data, selected)
    }

    const handleChange = (e) => {
        const { value, name } = e.target
        setValues(prevState => ({ ...prevState, [name]: value }))
    }

    return (
        <>
            <div className='align-grid'>
                <div className='grid'>
                    <div className="register-title">
                        Please input your data in the form below :
                    </div>
                    <div className="register-container">
                        <div className="register-input">
                            <form onSubmit={(e) => handleSubmit(e, values)} >
                                <label htmlFor="name" className="label">Name</label>
                                <input type="text" name="name" value={values?.name} onChange={handleChange} required />
                                <label htmlFor="address" className="label">Address</label>
                                <input type="text" name="address" value={values?.address} onChange={handleChange} required />
                                <label htmlFor="email" className="label">Email</label>
                                <input type="email" name="email" value={values?.email} onChange={handleChange} required />
                                <label htmlFor="membership" className="label">Selected Membership</label>
                                <input type="text" name="membership" value={selected.name} readOnly required />
                                <label htmlFor="price" className="label">Price</label>
                                <input type="text" name="price" value={`$${selected.price}`} readOnly required />
                                <input type="submit" value="Submit" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )

}
