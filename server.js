const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();
const port = process.env.PORT || 8080;
const db = require('./server/db/database');
const cors = require('cors')
const deduplicate = require('./server/service/cartSvc')
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: true })

app.use(express.static(path.join(__dirname, 'build')));

app.use(cors());

app.get('/health', function (req, res) {
  return res.send('ok');
});

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});


// Get product info API
app.get('/product', function (req, res) {
  db.all("SELECT * FROM product WHERE type !='B' AND price > 20", (err, rows) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    res.status(200).json({ "data": rows });
  });
})


//Get membership info 
app.get('/membership', function (req, res) {
  let sql = `SELECT * FROM membership`;
  db.all(sql, (err, rows) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    res.status(200).json({ "data": rows });
  })

})


// Add product to cart API with query params productId
app.post('/cart', function (req, res) {

  if (typeof req.query.productId === "undefined") {
    res.status(400).json({ "error": "Missing query param - productId" });
    return;
  }

  db.run(`INSERT INTO cart_item (product_id) VALUES (${req.query.productId})`, (err) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    db.all(`SELECT
            p.id,
            p.name, 
            p.price,
            p.description,
            p.type,
            COUNT(*) as count
          FROM
            cart_item ct
          INNER JOIN product p ON
            p.id = ct.product_id
          GROUP BY
            p.id
          HAVING
            count > 1
          ORDER BY
            p.name DESC;
    `, (err, rows) => {
      res.status(201).json({ "data": rows });
    })

  })
})


// Register Membership
app.post('/register', urlencodedParser, function (req, res) {

  if (typeof req.query.membership === "undefined") {
    res.status(400).json({ "error": "Missing query param - productId" });
    return;
  }

  db.run(`INSERT INTO member(membership_id, email, address, name ) VALUES (${req.body.membership_id}, ${req.body.email}, ${req.body.address}, ${req.body.name})`, (err) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    db.all(`SELECT
            m.id,
            m.name, 
            m.email,
            m.address,
            ms.name as membership_name,
          FROM
            member m
          INNER JOIN membership ms ON
            m.membership_id = ms.id
          WHERE m.membership_id = ${req.body.membership_id}
          ORDER BY
            m.name DESC;
    `, (err, rows) => {
      res.status(201).json({ "data": rows });
    })

  })
})

// Get Member
app.post('/member', function (req, res) {

  if (typeof req.query.membership === "undefined") {
    res.status(400).json({ "error": "Missing query param - membershipId" });
    return;
  }

  db.all(`SELECT
            m.id,
            m.name, 
            m.email,
            m.address,
            ms.name as membership_name,
          FROM
            member m
          INNER JOIN membership ms ON
            m.membership_id = ms.id
          WHERE m.membership_id = ${req.query.membership}
          ORDER BY
            m.name DESC;
    `, (err, rows) => {
    res.status(201).json({ "data": rows });
  })
})



// Get cart's product
app.get('/cart', function (req, res) {
  db.all(`SELECT
            p.id,
            p.name, 
            p.price,
            p.description,
            p.type,
            COUNT(*) as count
          FROM
            cart_item ct
          INNER JOIN product p ON
            p.id = ct.product_id
          GROUP BY
            p.id
          HAVING
            count > 1
          ORDER BY
            p.name DESC;
  `, (err, rows) => {

    const result = deduplicate(rows);

    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
    res.status(200).json({ "data": result });
  })
})

//Delet cart's prodcut 
app.delete('/cart', function (req, res) {
  db.run(`DELETE FROM cart_item`, (err) => {
    if (err) {
      res.status(400).json({ "error": err.message });
      return;
    }
  })

  db.all(`SELECT
            p.id,
            p.name, 
            p.price,
            p.description,
            p.type,
            p.criteria,
            COUNT(*) as count
          FROM
            cart_item ct
          INNER JOIN product p ON
            p.id = ct.product_id
          GROUP BY
            p.id
          HAVING
            count > 1
          ORDER BY
            p.name DESC;
    `, (err, rows) => {
    res.status(201).json({ "data": rows });
  })

})


app.listen(port, () => {
  console.log(`Start listening on port ${port}...`)
});

module.exports = { app };



